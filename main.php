<?php
  require_once 'Includes/login_checker.php'
 ?>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/medias.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="JS/main.js"></script>
    <meta charset="utf-8">
    <title>
      QuizR
    </title>
  </head>
  <body>

      <div>
       <div class="topnav" id="myTopnav">
         <a href="#home">Home</a>
         <a href="#words">Words</a>
         <a href="#quizes">Quizes</a>
         <a href="#materials">Materials</a>
         <a href="javascript:void(0);" class="icon" id="toggle">&#9776;</a>
         <?php echo "<p>" . $_SESSION['username'] . "</p>";?></p>
       </div>

       <div class="content_container">
         <h1 id = "title">Main</h1>
       </div>

   </div>
  </body>
</html>
