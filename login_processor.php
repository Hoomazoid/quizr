<?php
  header('Content-Type: application/json;  charset=utf-8');
  require_once('Includes/database.php');


  if(!isset($_POST['user']) || !isset($_POST['pass']))
  {
    echo "User or pass empty";
    die;
  }

  $usr = pg_escape_string($db, $_POST['user']);
  $pass = pg_escape_string($db, $_POST['pass']);

  $sql = "SELECT * FROM users WHERE email='" . $usr . "'";
  $ret = pg_query($db, $sql);
  if(!$ret){
      echo "Uh Oh some error occured :(";
      die;
  }
  if(pg_num_rows($ret) != 1)
  {
    $response = array('response'=>'Username does not exist');
  }else{

    $resultArray = pg_fetch_assoc($ret);

    $userMail = $resultArray["email"];
    $dbPwdHash = $resultArray["password"];
    $dbIsActivated = $resultArray["is_activated"];

    if(password_verify($pass, $dbPwdHash) && $userMail === $usr)
    {
      if(!$dbIsActivated || $dbIsActivated === 'f' || $dbIsActivated === 'false')
      {
        $response = array('response'=>'Account not activated');
      }else{
        session_start();
        $_SESSION["username"] = $userMail;
        $response = array('response'=>'true');
      }
    }else{
      $response = array('response'=>'Username or password incorrect');
    }
  }

    echo json_encode($response);

 ?>
