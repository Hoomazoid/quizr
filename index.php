<?php
session_start();
if (isset($_SESSION['username'])) {
    header("location:/Quizr/main.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <title>
      QuizR
    </title>
  </head>
  <body>
    <form>
      <input type="email" id="userMail" placeholder="Email">
      <br />
      <input type="password" id="userPass" placeholder="Password">
      <br />
      <input type="button" id = "submit" value = "Login">
      <br />
      <div id="errorBody" ></div>
    </form>

    <a href="/Quizr/register.php">Sign Up</a>

    <script src="JS/login.js"></script>
  </body>
</html>
