$(document).ready(function () {
    "use strict";


    $("#submit").click(function () {
      var userName = $('#userMail').val();
      var pwd = $('#userPass').val();
      var secPass = $('#secPass').val();
      if(pwd.length < 6)
      {
        $("#errorBody").html("Password must be at least 6 characters long");
      }else if(pwd !== secPass)
      {
        $("#errorBody").html("Passwords don't match");
      }
      performRegistrationAjax(userName, pwd, secPass);

      return false;
    });
});

function performRegistrationAjax(userName, pwd, secPass)
{
  $.ajax({
      type: "POST",
      url: "registration_processor.php",
      data: "user=" + userName + "&pass=" + pwd + "&pass2=" + secPass,
      dataType: 'json',
      success: function (html) {
        console.log(html);

          if(html.response === "true")
          {
            alert('Success');
            location.assign("/Quizr/activate_explain.php");
          }else{
            $("#errorBody").html(html.response);
          }
      },
      error: function (textStatus, errorThrown) {
        console.log("რაღაც ნიტოა სცადეთ ხელახლა. " + textStatus + " " + errorThrown)
      },
      beforeSend: function () {
      }
  });
}
