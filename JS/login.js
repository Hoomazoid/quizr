$(document).ready(function () {
    "use strict";


    $("#submit").click(function () {
      var userName = $('#userMail').val();
      var pwd = $('#userPass').val();


      if(userName.length > 0 && pwd.length > 0)
      {
        performLoginAjax(userName, pwd);
      }else{
        $("#errorBody").html("Please fill in all the inputs");
      }

      return false;
    });
});

function performLoginAjax(userName, pwd)
{
  $.ajax({
      type: "POST",
      url: "login_processor.php",
      data: "user=" + userName + "&pass=" +pwd,
      dataType: 'JSON',
      success: function (html) {
        console.log(html);

          if(html.response === "true")
          {
            alert('success');
            location.reload();
          }else{
            $("#errorBody").html(html.response);
          }
      },
      error: function (textStatus, errorThrown) {
        $("#errorBody").html("რაღაც ნიტოა, დაუკავშირდით ადმინსტრატორს");
      },
      beforeSend: function () {
      }
  });
}
