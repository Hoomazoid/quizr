<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <title>QuizR - Registration</title>
  </head>
  <body>
    <form>
      <input type="email" id="userMail" placeholder="Email">
      <br />
      <input type="password" id="userPass" placeholder="Password">
      <br />
      <input type="password" id="secPass" placeholder="Repeat Password">
      <br />
      <input type="button" id = "submit" value = "Register">
      <br />
      <div id="errorBody" ></div>
    </form>
    <script src="JS/register.js"></script>
  </body>
</html>
