<?php
  header('Content-Type: application/json; charset=utf-8');
  require_once('/Includes/database.php');

  if(!isset($_POST['user']) || !isset($_POST['pass']) || !isset($_POST['pass2']))
  {
    $res = array("response" => "Enough data not set");
    echo json_encode($res);
    die;
  }

  $usr = pg_escape_string($db, $_POST['user']);
  $pass = pg_escape_string($db, $_POST['pass']);
  $secPass = pg_escape_string($db, $_POST['pass2']);

  if($secPass !== $pass)
  {
    $res = array("response" => "Passwords don't match");
  }else if(strlen($pass) < 6)
  {
    $res = array("response" => "Password too short");
  }else if(!filter_var($usr, FILTER_VALIDATE_EMAIL) == true)
  {
    $res = array("response" => "Email not valid");
  }else{
    $passHash = password_hash($pass, PASSWORD_DEFAULT);
    if(!$passHash)
    {
      $res = array("response" => "Hash failed");
      echo json_encode($res);
      die;
    }

    $sql = "SELECT * FROM users WHERE email = '" . $usr . "'";
    $ret = pg_query($db, $sql);
    if(pg_num_rows($ret) > 0)
    {
      $res = array("response" => "User already registered");
      echo json_encode($res);
      die;
    }
    $activationHash = md5($usr . rand(1, 10000));
    $activationHash = pg_escape_string($db, $activationHash);

    $sql = "INSERT INTO users (email, password, activation_hash, is_activated)
    VALUES('$usr', '$passHash', '$activationHash', 'FALSE')";
    $ret = pg_query($db, $sql);

    if(!$ret){
      $res = array("response" => "Could not register an user");
    }else{
      ActivationMailer::sendActivationMailTo($usr, $activationHash);
      $res = array("response" => "true");
    }
    echo json_encode($res);
  }
 ?>
